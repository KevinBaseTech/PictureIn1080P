package chiu.kevin.fullscreenpic;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapRegionDecoder;
import android.graphics.Point;
import android.graphics.Rect;
import android.os.Bundle;
import android.util.Log;
import android.view.Display;
import android.widget.ImageView;

import java.io.IOException;
import java.io.InputStream;

public class MainActivity extends Activity {
    private Bitmap bitmap;
    private static final String TAG = "MainActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        InputStream inputStream = null;
        try {
            // Get image via Stream
            inputStream = getAssets().open("testpattern_1080.png");
            BitmapFactory.Options opt = new BitmapFactory.Options();
            opt.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(inputStream,null,opt);

            // Get image width and height
            int imageHeight = opt.outHeight;
            int imageWidth = opt.outWidth;

            // Get screen width and height
            Display display = getWindowManager().getDefaultDisplay();
            Point point = new Point();
            display.getRealSize(point);
            int screenHeight = point.y;
            int screenWidth = point.x;
            opt.inJustDecodeBounds = false;

            Log.d(TAG, "onCreate:\nimageHeight=" + imageHeight + "\nimageWidth=" + imageWidth + "\nscreenHeight=" + screenHeight + "\nscreenWidth=" + screenWidth);

            // Find imageView
            ImageView iv = (ImageView) findViewById(R.id.iv);

            // Use BitmapRegionDecoder to decode the stream. This way will keep the picture quality.
            BitmapRegionDecoder bitmapRegionDecoder = BitmapRegionDecoder.newInstance(inputStream, false);
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inPreferredConfig = Bitmap.Config.RGB_565;
            bitmap = bitmapRegionDecoder.decodeRegion(new Rect(0, 0, screenWidth, screenHeight), options);
            iv.setImageBitmap(bitmap);

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (inputStream != null) {
                try {
                    inputStream.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (bitmap != null) {
            bitmap.recycle();
            bitmap = null;
        }
    }
}
